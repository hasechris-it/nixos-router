{ config, pkgs, ... }:

{

  users.users.chris = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [ config.sshKeys.chris.desktop ];
  };
  users.users.root = {
    openssh.authorizedKeys.keys = [ config.sshKeys.chris.desktop ];
  };

}
