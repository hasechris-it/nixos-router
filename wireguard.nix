{ config, pkgs, ... }:

{
	config.networking.wireguard.interfaces.wg0 = {
		ips = [ "10.253.0.42/24" ];
		listenPort = 51820;
		allowedIPsAsRoutes = true;
		privateKeyFile = "/data/nixoscfg/wg.private";
		peers = [
			
			#{
			#	# handy.hasiatthegrill.net
			#	publicKey = "kle2jK61jEWoEHPPunopM1xnP9z/bQwq7L0tx0NY1mQ=";
			#	allowedIPs = [ "10.253.0.2/32" ];
			#	persistentKeepalive = 25;
			#}
			{
				# m1k3y
				endpoint = "rwg.m1k3y.de:31250";
				publicKey = "VXeqzgdwNgiKkLLd7r1SUV3zw3tcAH1uarLSUYdteRo=";
				persistentKeepalive = 25;
				allowedIPs = [ "10.253.0.69/32" "10.253.69.0/24" ];
			}
			{
				# exec
				endpoint = "execnet.de:51820";
				publicKey = "Laoqql/uVjsOv3UX1NLyofPM+vKOgZhOb7DBIUYLy2M=";
				persistentKeepalive = 25;
				allowedIPs = [ "10.253.0.66/32" "10.253.66.0/24" ];
			}
		];
	};
	config.networking.wireguard.interfaces.wg1 = {
		ips = [ "192.168.4.5/24" ];
		listenPort = 51821;
		allowedIPsAsRoutes = true;
		privateKeyFile = "/data/nixoscfg/wg1.private";
		peers = [
			{
				endpoint = "wg.hasiatthegrill.net:51820";
				publicKey = "uIgD/fQTcqVT6toDwoJOfkPNJc1+MoOp2KprZktriTI=";
				persistentKeepalive = 25;
				allowedIPs = [ "192.168.4.0/24" ];
			}
		];
	};
}
