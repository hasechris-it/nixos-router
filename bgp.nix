{ config, pkgs, ... }:

{
	
	services.quagga = {
		zebra = {
			enable = true;
			vtyListenPort = 2601;
			configFile = "/data/quagga/zebra.conf";
		};
		bgp = {
			enable = true;
			vtyListenPort = 2605;
			configFile = "/data/quagga/bgpd.conf";
		};
	};
}


