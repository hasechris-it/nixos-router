{ config, pkgs, ... }:

{
	services.openssh = {
		enable = true;
		challengeResponseAuthentication = false;
		passwordAuthentication = false;
		permitRootLogin = "no";
		#extraConfig = ''UsePAM yes'';
		#authorizedKeysFiles = [ ".ssh/authorized_keys" ];
	};
	
	security.pam.enableSSHAgentAuth = true;
}
