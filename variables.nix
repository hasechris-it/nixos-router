{ config, pkgs, lib, ... }:

with lib;{
	
	options.sshKeys.chris.desktop = lib.mkOption {
		type = types.str;
	};
	options.packages.unfree = mkOption {
		type = types.listOf types.str;
		description = "foo";
	};
}
